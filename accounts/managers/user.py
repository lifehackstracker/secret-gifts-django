# -*- coding: utf-8 -*-
# python imports
from __future__ import unicode_literals

# lib imports
from django.contrib.auth.models import BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.db import models


class UserQueryset(models.QuerySet):
    # NOTE: Querysets can define special name to chainable methods
    # def employees(self):
    #     return self.filter(user_type=self.model.EMPLOYEE)
    pass


class UserManager(BaseUserManager):
    def get_queryset(self):
        return super(UserManager, self).get_queryset()

    def create_superuser(
        self, username, password, email=None, phone_no=None, **extra_fields
    ):
        """
        Special Manager method for createsuperuser command
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superuser must have is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superuser must have is_superuser=True."))
        return self.create_user(
            username, password, email, phone_no, **extra_fields
        )

    def create_user(self, username, password, email, phone_no, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        NOTE: We can enforce database level business logic inside Manager methods
        """
        if not username:
            raise ValueError("The given username must be set")
        email = self.model.normalize_email(email)
        phone = self.model.normalize_phone_no(phone_no)
        username = self.model.normalize_username(username)
        user = self.model(
            username=username, phone_no=phone_no, email=email, **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
