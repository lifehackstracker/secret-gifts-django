# third party libraries
from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _
from phonenumbers import parse, format_number, PhoneNumberFormat
from phonenumbers.phonenumberutil import NumberParseException

# project imports
from accounts.managers.user import UserManager, UserQueryset


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user table - inherits Django's base user.
    """

    first_name = models.CharField(
        max_length=50,
        verbose_name='First Name'
    )

    last_name = models.CharField(
        max_length=50,
        verbose_name='Last Name'
    )

    phone_no = models.CharField(
        max_length=11,
        validators=[
            RegexValidator(
                regex='^([0-9]{10,11})$',
                message="""Phone number must be 10-11 digits.""",
                code='invalid_phone_number'
            ),
        ],
        null=True,
        blank=True,
        verbose_name='Phone number'
    )

    username = models.CharField(
        max_length=100,
        unique=True,
        db_index=True,
        verbose_name='Username'
    )

    is_active = models.BooleanField(
        default=False,
        db_index=True,
        verbose_name='Active'
    )

    extra_data = JSONField(
        verbose_name='Extra data',
        default=dict
    )

    is_deleted = models.BooleanField(
        default=False,
        verbose_name='Deleted'
    )

    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_(
            "Designates whether the user can log into this admin site."
        ),
    )

    email = models.EmailField(_("email address"), blank=True, db_index=True)

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Create datetime'
    )

    last_modified = models.DateTimeField(
        auto_now=True,
        verbose_name='Last modified'
    )

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = [
        "first_name",
        "last_name",
    ]

    objects = UserManager.from_queryset(UserQueryset)()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return '{} {} - {}'.format(self.first_name, self.last_name, self.id)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = f"{self.first_name} {self.last_name}"
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    @classmethod
    def normalize_email(cls, email):
        """
        Normalize the email address by removing spaces and lowercasing it.
        """
        return email.strip().lower() if email else ""

    @classmethod
    def normalize_phone_no(cls, phone_no):
        """
        Normalize the phone number by formatting to INTERNATIONAL standard.
        """
        try:
            phone = parse(phone_no)
            return format_number(phone_no, PhoneNumberFormat.INTERNATIONAL)
        except NumberParseException:
            return ""