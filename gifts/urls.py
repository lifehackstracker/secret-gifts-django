from django.urls import path

from .views import room

urlpatterns = [
    path('', room.room_select, name='home'),
    path('dashboard/', room.dashboard, name='dashboard'),
    path('room/', room.create, name='list_create_room'),
    path('coming_soon/', room.coming_soon, name="coming_soon")
]