# third party libraries
from django.db import models

# project imports
from gifts.models.room import Room


class Participant(models.Model):
    first_name = models.CharField(max_length=512)
    last_name = models.CharField(max_length=512, null=True, blank=True)
    room = models.ForeignKey(
        to=Room, on_delete=models.CASCADE, related_name="room_participants"
    )
    email = models.EmailField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Participant'
        verbose_name_plural = 'Participant'

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
