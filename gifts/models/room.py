# third party libraries
from django.db import models


class Room(models.Model):

    name = models.CharField(max_length=6, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    participant_count = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Room'
        verbose_name_plural = 'Rooms'

    def __str__(self):
        return f"{self.name}"
