# third party libraries
from django.db import models

# project imports
from gifts.models.room import Room
from gifts.models.participant import Participant


class ParticipantMapping(models.Model):
    from_participant = models.ForeignKey(
        to=Participant,
        on_delete=models.CASCADE,
        related_name="mapping_from_participants"
    )
    to_participant = models.ForeignKey(
        to=Participant, on_delete=models.CASCADE,
        related_name="mapping_to_participants"
    )
    room = models.ForeignKey(to=Room, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Participant'
        verbose_name_plural = 'Participant'

    def __str__(self):
        return f"{self.from_participant} to {self.to_participant}"
