from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from gifts.models import Room
import json

@csrf_exempt
def room_select(request):
    return render(request, "gifts/room/room_select.html")

@csrf_exempt
def dashboard(request):
    return render(request, "gifts/room/dashboard.html")

@csrf_exempt
def create(request):
    body = json.loads(request.body)
    name = body.get("group_name")
    try:
        r = Room.objects.get(name=name)
        if r:
            return HttpResponse(
                json.dumps({"status": 409}),
                content_type='application/json',
                status=409
            )
    except Room.DoesNotExist:
        r = Room.objects.create(name=name)
        return HttpResponse(
            json.dumps({"status": 201}),
            content_type='application/json',
            status=201
        )

def coming_soon(request):
    return render(request, "coming_soon.html")