function createRoom() {
    console.log("Hello world!");
    console.log(document.getElementById("create-group-text").value);
    let $ = jQuery.noConflict();
    const headers = {
        'Content-Type': 'application/json'
    }
    axios.request({
        method: 'POST',
        url: `/room/`,
        headers: headers,
        data: {
            group_name: document.getElementById("create-group-text").value
        },
    }).then((response) => {
      console.log(response.status);
      window.location.href = "/coming_soon";
    }, (error) => {
      console.log(error);
      console.log("here");
    });

}